package com.if26.tweetit_parquet;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

/**
 * Created by Nico on 05/11/13.
 */
public class TweetController extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tweet_it, null);

        Bundle args = getArguments();
        if(args != null) {
            String tweetContent = args.getString("tweetContent");
            if ( tweetContent != null) {
                EditText contentZone = (EditText)view.findViewById(R.id.textArea);
                contentZone.setText(tweetContent);
            }
        }
        return view;
    }
}
