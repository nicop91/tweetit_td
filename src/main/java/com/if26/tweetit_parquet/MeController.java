package com.if26.tweetit_parquet;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Nico on 04/11/13.
 */
public class MeController extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.me, null);
        String[] tweets = getResources().getStringArray(R.array.tweet_list_me);
        ListView list = (ListView)view.findViewById(R.id.tweetsListMe);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tweets);
        list.setAdapter(arrayAdapter);

        setListElementClickAction(list);

        return view;
    }

    private void setListElementClickAction(ListView list) {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                String tweetContent = "RT: " + adapter.getItemAtPosition(position);

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                TweetController tweetFragment = new TweetController();
                Bundle args = new Bundle();
                args.putString("tweetContent", tweetContent);
                tweetFragment.setArguments(args);

                fragmentTransaction.replace(R.id.tab_content_container, tweetFragment, "HELLO");
                fragmentTransaction.commit();
            }
        });
    }
}
