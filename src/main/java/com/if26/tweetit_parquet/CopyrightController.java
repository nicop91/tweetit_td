package com.if26.tweetit_parquet;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Nico on 22/10/13.
 */
public class CopyrightController extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.copyright, container, false);

        // Create onClick action on button (to display copyright info in a popup)
        view.findViewById(R.id.copyright_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.copyright_content));
                builder.create().show();
            }
        });
        return view;
    }
}
