package com.if26.tweetit_parquet;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Nico on 05/11/13.
 */
public class FriendsController extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.friends, null);
        String[] tweets = getResources().getStringArray(R.array.tweet_list_friends_followers);
        ListView list = (ListView)view.findViewById(R.id.tweetsListFriends);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tweets);
        list.setAdapter(arrayAdapter);

        return view;
    }
}
