package com.if26.tweetit_parquet;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Nico on 22/10/13.
 */
public class TabsController extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tabs, container, false);

        view.findViewById(R.id.button_me).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                MeController meFragment = new MeController();
                fragmentTransaction.replace(R.id.tab_content_container, meFragment, "HELLO");
                fragmentTransaction.commit();
            }
        });

        view.findViewById(R.id.button_friends).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                FriendsController friendsFragment = new FriendsController();
                fragmentTransaction.replace(R.id.tab_content_container, friendsFragment, "HELLO");
                fragmentTransaction.commit();
            }
        });

        view.findViewById(R.id.button_tweetit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                TweetController tweetFragment = new TweetController();
                fragmentTransaction.replace(R.id.tab_content_container, tweetFragment, "HELLO");
                fragmentTransaction.commit();
            }
        });

        return view;
    }

}
